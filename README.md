# README #

Simple desktop chat example application based on Scala & akka & ScalaFx.
It was created to test capabilities of Scala infrastructure.

You can download compiled jars in 'Downloads' section.
Alternatively clone the project and compile & run it yourself with sbt run (JDK8 requered).

How to run: first run `StartServer.bat`, then run `StartClient.bat` several times.

Alternatively server command line : `java -jar scalafxakkachat.jar`

Client command line : `start javaw -cp scalafxakkachat.jar my.MyClientApp`