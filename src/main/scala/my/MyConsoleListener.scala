package my

import java.io.BufferedReader

import akka.actor.{Actor, ActorLogging, ActorRef, Props}

object MyConsoleListener{
	def props(parentActor : ActorRef, stdIn : BufferedReader):Props={
	    Props(new MyConsoleListener(parentActor, stdIn))
	}
}

class MyConsoleListener(parentActor : ActorRef, stdIn : BufferedReader) extends Actor with ActorLogging{

	def receive: Actor.Receive ={
		case MyStartServerConsole =>
			onStartConsole()
	}

	def onStartConsole() : Unit={
		println("ScalaFxAkkaChat server app...")

		println("Type ':quit' to exit")

		var conditionVar = true
		while (conditionVar) {
			val line = stdIn.readLine()
			if (line == ":quit"){
				conditionVar = false
				parentActor !  MyCloseApp
			}
		}
		println("good bye!")
		context.stop(self)
	}

}