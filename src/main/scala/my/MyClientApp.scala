package my

import akka.actor.{ActorRef, ActorSystem, Props}

import scalafx.application.JFXApp

object MyClientApp extends JFXApp{
//	private def onSettings(): Unit ={
//		val result : Option[MyHelloResult] = MyHelloDialog.showAndWait()
//		result match{
//			case Some(MyHelloResult(name,hostName)) =>
//				startClient(name, hostName)
//			case None =>
//				javafx.application.Platform.exit()
//		}
//	}

	private val result : Option[MyHelloResult] = MyHelloDialog.showAndWait()
	private var actorSystem : ActorSystem = _
	private var clientActor : ActorRef = _

	if (result.isEmpty){
		javafx.application.Platform.exit()
	}else{
		MyMainStage.title = s"ScalaFX Akka Chat, ${result.get.clientName}"
		
		actorSystem = ActorSystem("ClientSystem")

		clientActor = actorSystem.actorOf(MyClient.props(result.get.clientName, result.get.hostName),"MyClient")

		MyMainStage.clientActor = clientActor
		stage = MyMainStage

		clientActor ! MyClientConnect

		stage.show()
	}

}
