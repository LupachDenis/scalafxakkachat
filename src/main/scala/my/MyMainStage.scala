package my

import akka.actor.ActorRef
import scalafx.Includes._
import scalafx.application.JFXApp.PrimaryStage
import scalafx.collections.ObservableBuffer
import scalafx.geometry.Insets
import scalafx.scene.Scene
import scalafx.scene.control._
import scalafx.scene.input.{KeyCode, KeyEvent}
import scalafx.scene.layout.{HBox, Priority, VBox}
import javafx.scene.{control => jfxsc}

object MyMainStage extends PrimaryStage {

	this.hide()

	var clientActor : ActorRef = _
	var connected = false
	private val participantsListView = new ListView[MyClientIdName]{}
	private val participantsBuffer = new ObservableBuffer[MyClientIdName]() //ObservableBuffer(Seq("x1", "x2"))

	if (false) {
		// javafx style
		participantsListView.cellFactory = (_: ListView[MyClientIdName]) => {
			new ListCell[MyClientIdName](new jfxsc.ListCell[MyClientIdName] {
				override def updateItem(clientIdName: MyClientIdName, empty: Boolean): Unit = {
					super.updateItem(clientIdName, empty)

					if (empty || clientIdName == null)
						setText("")
					else
						setText(clientIdName.name)
				}
			})
		}
	}else {
		// scalafx style
		participantsListView.cellFactory = {
			_ =>
				new ListCell[MyClientIdName] {
					item.onChange {
						(_, _, newValue) =>
							if (newValue == null)
								text = ""
							else
								text = newValue.name
					}
				}
		}
	}

	participantsListView.items = participantsBuffer

	private val textArea = new TextArea{
		editable = false

		vgrow = Priority.Always
		hgrow = Priority.Always
	}
	//textArea.setTextFormatter()

	private val textInput = new TextField(){
		hgrow = Priority.Always

//		onKeyPressed = (keyEvent: KeyEvent)=> {
//			if (keyEvent.code == KeyCode.Enter)
//				onSessionClosed()
//		}

		onKeyPressed = (_: KeyEvent).code match {
			case KeyCode.Enter => onClickButton()
			case _ =>
		}
	}

	private val clickButton = new Button("Click me!"){
		defaultButton = true
		disable = false
		onMouseClicked = handle{
			onClickButton()
			textInput.requestFocus()
		}
	}

	private def onClickButton(): Unit ={
		//textArea.appendText("sampleText\n")
		if (textInput.text().isEmpty)
			return
		if (clientActor == null)
			return

		clientActor ! MyTextFromClient(textInput.text())
		textInput.setText("")
	}

	def onHelloFromServer(message : MyHelloFromServerMessage): Unit ={
		clickButton.disable = false
		connected = true

		participantsBuffer.clear()
		for ( client <- message.clients )
			participantsBuffer += client
	}

	def onSessionClosed(): Unit ={
		clickButton.disable = true
		connected = false
	}

	def onDisplayableMessage(message : MyDisplayableMessage): Unit ={

		def appendText(text : String): Unit ={
			if (text.isEmpty)
				return
			textArea.appendText(text)
		}

		message match{
			case c: MyTextMessage =>
				appendText(s"${c.client.name}> ${c.message}\n")
				
			case c: MyClientConnectedMessage =>
				appendText(s"${c.client.name} connected...\n")
				if (connected) {
					participantsBuffer += c.client
				}

			case c: MyClientDisconnectedMessage =>
				appendText(s"${c.client.name} disconnected...\n")
				if (connected) {
					val index = participantsBuffer.indexWhere { x: MyClientIdName => x.clientId == c.client.clientId }
					if (index >= 0)
						participantsBuffer.remove(index)
				}
		}
	}

	width = 800
	height = 600
	minWidth = 400
	minHeight = 300
	scene = new Scene{
		root = new HBox{
			padding = Insets(MyUtil.defaultSpacing)
			spacing = MyUtil.defaultSpacing
			children = Seq(
				participantsListView,
				new VBox{
					hgrow = Priority.Always
					spacing = MyUtil.defaultSpacing
					children = Seq(
						textArea,
						new HBox{
							spacing = MyUtil.defaultSpacing
							children = Seq(
								textInput,
								clickButton
							)
						}
					)
				}
			)
		}
	}

	onCloseRequest = handle{
		if (clientActor != null)
			clientActor ! MyCloseApp
	}
	
	textInput.requestFocus()
}
