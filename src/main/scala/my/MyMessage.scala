package my

import java.util.UUID

case class MyClientIdName(clientId : Int, name : String) extends Serializable

// ConsoleListener
case object MyStartServerConsole
case object MyCloseApp

// From ClientActor
case class MyUserInput(msg : String)

// From Session
case class MySessionClosed(clientId : Int)

// To Server/Client
case object MyServerBind
case object MyClientConnect


// Session <-> Server / Client

trait MyMessage extends Serializable

trait MyInternalMessage extends MyMessage

case object MyKeepAliveMessage extends MyInternalMessage

case class MyHelloFromClientMessage( clientName : String ) extends MyInternalMessage
case class MyHelloFromServerMessage( serverUuid : UUID, clients : List[MyClientIdName] ) extends MyInternalMessage

case class MyTextFromClient( text : String ) extends MyInternalMessage

trait MyDisplayableMessage extends MyMessage{
	def messageId : Int
}

case class MyTextMessage(messageId : Int, client : MyClientIdName, message: String) extends MyDisplayableMessage
case class MyClientDisconnectedMessage(messageId : Int, client : MyClientIdName) extends MyDisplayableMessage
case class MyClientConnectedMessage(messageId : Int, client : MyClientIdName) extends MyDisplayableMessage

object MyMessageConfig {
	val protocolHeader : Short = 0x1F1F.toShort
	val protocolVersion : Short = 1
	val protocolHeaderLength : Int = 2+2+2
	val maxTextLength = 0x7fff
	val maxNameLength = 0x100
	
}
