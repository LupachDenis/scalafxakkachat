package my

import java.net.InetSocketAddress
import java.util.UUID

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.io.{IO, Tcp}

import scala.collection.mutable
import scala.concurrent.duration._

object MyServer{
	def props():Props={
		Props(new MyServer)
	}
}

class MyServer extends Actor with ActorLogging{

	import context.system

	private val expectedSessions = mutable.HashMap[Int,ActorRef]()
	private val sessions = mutable.HashMap[Int, (MyClientIdName, ActorRef)]()
	private val sessionsByActorRef = mutable.HashMap[ActorRef, Int]()
	private var currentMaxSession = 0

	val serverUuid : UUID = UUID.randomUUID()
	private val messageArchive = mutable.Queue[MyDisplayableMessage]()
	private var currentMessageId = 0
	private var firstBindingFail = true

	private def nextMessageId():Int={
		currentMessageId += 1
		currentMessageId
	}

	private def tryToBind(): Unit ={

		IO(Tcp) ! Tcp.Bind(self, new InetSocketAddress("0.0.0.0", MyUtil.defaultPort))
	}

	override def receive : Receive = {
		// Start:
		case MyServerBind => tryToBind()
		// Tcp :
		case Tcp.Bound(address) => log.info(s"Bound on ${address.getHostName}:${address.getPort}")
		case c @ Tcp.CommandFailed(_ : Tcp.Bind) => onBindingFailed(c)
		case c : Tcp.Connected => onConnected(c)

		// Session :
		case c : MySessionClosed => onSessionClosed(c.clientId)

		// ConsoleListener :
		case MyCloseApp => onCloseApp()

		// Messages :
		case message : MyHelloFromClientMessage => onHelloFromClientMessage(message)
		case message : MyTextFromClient => onTextFromClient(message)

		case x : AnyRef => log.info(x.toString)

	}

	private def onBindingFailed( c : Tcp.CommandFailed ): Unit ={
		if (firstBindingFail) {
			log.info(s"Binding failed. ${c.toString()}")
			firstBindingFail = false
		}
		context.system.scheduler.scheduleOnce(10 seconds, self, MyServerBind)(context.dispatcher)
	}

	private def onConnected(c : Tcp.Connected){
		log.info(s"Connected on ${c.remoteAddress.getHostName}:${c.remoteAddress.getPort}")
		currentMaxSession += 1
		val session = context.actorOf(MySession.props(sender(), self, currentMaxSession), s"Session_$currentMaxSession")
		expectedSessions += (currentMaxSession -> session)
		sessionsByActorRef += (session -> currentMaxSession)
		sender() ! Tcp.Register(session)
	}

	private def senderSessionInfo() : Option[(MyClientIdName, ActorRef)] ={
		val intOpt = sessionsByActorRef.get(sender())
		if (intOpt.isEmpty)
			return None
		sessions.get(intOpt.get)
	}

	private def onHelloFromClientMessage(message : MyHelloFromClientMessage): Unit ={
		val intOpt = sessionsByActorRef.get(sender())
		if (intOpt.isEmpty)
			return

		expectedSessions -= intOpt.get
		val clientIdName = MyClientIdName(intOpt.get, message.clientName)

		sendDisplayableMessage( MyClientConnectedMessage(nextMessageId(), clientIdName) )

		sessions += (intOpt.get -> (clientIdName, sender()))

		for (message <- messageArchive)
			sender() ! message

		val clients : List[MyClientIdName] = (for ( (_,(clientIdNameI,_ )) <- sessions ) yield clientIdNameI)(collection.breakOut)
		sender() ! MyHelloFromServerMessage( serverUuid, clients )

	}

	private def onTextFromClient(message : MyTextFromClient): Unit ={
		val senderSession = senderSessionInfo()
		if (senderSession.isEmpty)
			return

		sendDisplayableMessage( MyTextMessage(nextMessageId(), senderSession.get._1, message.text) )
	}

	private def sendDisplayableMessage(message : MyDisplayableMessage): Unit ={
		messageArchive += message
		while(messageArchive.size > 40)
			messageArchive.dequeue()

		for ( (_, (_,actorRef) ) <- sessions){
			actorRef ! message
		}
	}

	private def onSessionClosed(clientId : Int): Unit ={
		expectedSessions -= clientId
		sessionsByActorRef -= sender()

		val optSessions = sessions.get(clientId)
		if (optSessions.isDefined){
			sessions -= clientId
			sendDisplayableMessage( MyClientDisconnectedMessage(nextMessageId(), optSessions.get._1 ) )
		}

		context.stop(sender())
	}

	private def onCloseApp():Unit ={
		context.stop(self)
		context.system.terminate()
	}

}
