package my

import java.net.InetSocketAddress
import java.util.UUID

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.io.{IO, Tcp}

import scala.concurrent.duration._
import scalafx.application.Platform

object MyClient{
	def props( name : String, hostName : String): Props ={
		Props(new MyClient(name, hostName))
	}
}

class MyClient( name : String, hostName : String) extends Actor with ActorLogging{
	import context.system

	private var session : ActorRef = _
	private var firstTryConnect = true
	private var serverUuid : UUID = _
	private var maxMessageId = 0

	private def tryToConnect() : Unit={
		IO(Tcp) ! Tcp.Connect(new InetSocketAddress(hostName, MyUtil.defaultPort))
	}

	override def receive : Receive ={
		case MyCloseApp => onCloseApp()

		// Start
		case MyClientConnect => tryToConnect()

		// Tcp :
		case c : Tcp.Connected => onConnected(c)
		case c @ Tcp.CommandFailed(_ : Tcp.Connect) => onConnectFailed(c)
		case _ : MySessionClosed => onSessionClosed()

		// From UI :
		case message : MyTextFromClient => onTextFromClient(message)

		// Messages :
		case message : MyHelloFromServerMessage => onHelloFromServerMessage(message)

		case message : MyDisplayableMessage => onDisplayableMessage(message)

	}

	private def onCloseApp(): Unit ={
		context.stop(self)
		context.system.terminate()
	}

	private def onConnected(c : Tcp.Connected): Unit ={
		println(s"Connected to ${c.remoteAddress.getHostName}:${c.remoteAddress.getPort}")
		if (session == null) {
			session = context.actorOf(MySession.props(sender(), self, 0), "MySession")
			sender() ! Tcp.Register(session)

			session ! MyHelloFromClientMessage(name)
		}
	}

	private def onConnectFailed(c : Tcp.CommandFailed): Unit ={
		if (firstTryConnect) {
			println(s"Connected failed. ${c.toString()}")
			firstTryConnect = false
		}
		context.system.scheduler.scheduleOnce(10 seconds, self, MyClientConnect)(context.dispatcher)
	}

	private def onSessionClosed(): Unit ={
		println(s"Disconnected from server")
		if (session != null) {
			context.stop(session)
			session = null
		}

		Platform.runLater{ MyMainStage.onSessionClosed() }

		tryToConnect()
	}

	private def onHelloFromServerMessage(message : MyHelloFromServerMessage): Unit ={
		if (message.serverUuid != serverUuid){
			serverUuid = message.serverUuid
			maxMessageId = 0
		}

		Platform.runLater{MyMainStage.onHelloFromServer(message)}
	}

	private def onDisplayableMessage(message : MyDisplayableMessage): Unit ={
		if (message.messageId <= maxMessageId)
			return
		maxMessageId = message.messageId

		Platform.runLater{MyMainStage.onDisplayableMessage(message)}
	}

	private def onTextFromClient(text : MyTextFromClient): Unit ={
		if (session != null)
			session ! text
	}
}
