package my

import scalafx.application.Platform
import scalafx.geometry.Pos
import scalafx.scene.control._
import scalafx.scene.layout.{GridPane, Priority}

case class MyHelloResult(clientName : String, hostName : String)

object MyHelloDialog extends Dialog[MyHelloResult] {

	private val popularNames = Array(
		"Noah",
		"Liam",
		"William",
		"Mason",
		"James",
		"Benjamin",
		"Jacob",
		"Michael",
		"Elijah",
		"Ethan",

		"Emma",
		"Olivia",
		"Ava",
		"Sophia",
		"Isabella",
		"Mia",
		"Charlotte",
		"Abigail",
		"Emily",
		"Harper"
	)

	title = "Hello!"

	private val grid = new GridPane()
	grid.hgap = MyUtil.defaultSpacing
	grid.vgap = MyUtil.defaultSpacing
	grid.maxWidth = Double.MaxValue
	grid.alignment = Pos.CenterLeft

	private val labelName = Label("Your name:")
	private val labelHostName = Label("Server host:")
	private val textFieldName = new TextField()
	textFieldName.text = popularNames( (MyUtil.defaultRandom.nextInt()&0xffff) % popularNames.length )
	private val textFieldHostName = new TextField()
	textFieldHostName.text = "localhost"

	grid.add(labelName, 0, 0)
	grid.add(textFieldName, 1, 0)
	grid.add(labelHostName, 0, 1)
	grid.add(textFieldHostName, 1, 1)

	for ( widget <- Seq(textFieldName, textFieldHostName) ){
		widget.minWidth = 150
		widget.hgrow = Priority.Always
	}

	dialogPane().setContent(grid)
	dialogPane().getButtonTypes.addAll(ButtonType.OK, ButtonType.Cancel)

	def showAndWait(): Option[MyHelloResult] = {
		super.showAndWait((x: MyHelloResult) => x).asInstanceOf[Option[MyHelloResult]]
	}

	resultConverter = (bt : ButtonType) => {
		bt match{
			case ButtonType.OK => MyHelloResult(textFieldName.text(),textFieldHostName.text())
			case _ => null
		}
	} : MyHelloResult

	Platform.runLater(textFieldName.requestFocus)
}