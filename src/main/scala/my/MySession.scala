package my

import java.io.{ByteArrayInputStream, ByteArrayOutputStream, ObjectInputStream, ObjectOutputStream}
import java.nio.ByteOrder

import akka.actor.{Actor, ActorLogging, ActorRef, Cancellable, Props, Terminated}
import akka.io.Tcp
import akka.io.Tcp.Write
import akka.util.{ByteIterator, ByteString, ByteStringBuilder}
import scala.concurrent.duration._

object MySession{
	def props(socketActor : ActorRef, server: ActorRef, clientId : Int): Props ={
		Props(new MySession(socketActor, server, clientId))
	}
}

class MySession
	(val socketActor : ActorRef, val chatServer : ActorRef, val clientId : Int)
	extends Actor with ActorLogging
{
	case object MyClosedByTimeout
	case object MySendKeepAlive
	
	private var messageLengthExpected = 0
	private var currentData : ByteString = ByteString()
	private var keepAliveScheduler : Cancellable = createKeepAliveScheduler()
	private val sendKeepAliveScheduler : Cancellable =
		if (clientId != 0) // client mode
			context.system.scheduler.schedule(19 seconds, 19 seconds, self, MySendKeepAlive)(context.dispatcher)
		else
			null

	private implicit val defaultByteOrder : ByteOrder = ByteOrder.LITTLE_ENDIAN

	context.watch(socketActor)

	override def receive: Receive = {
		case Tcp.Received(data) => processIncomingData(data)
		case message : MyMessage => sendMessage(message)
		case x : Tcp.ConnectionClosed => socketClosed()
		case x : Terminated =>
			log.info(x.toString)
			socketClosed()
		case MyClosedByTimeout =>
			log.info(s"Closed by timeout session $clientId")
			socketClosed()
		case MySendKeepAlive =>
			sendMessage(MyKeepAliveMessage)
		case x : AnyRef => log.info(x.toString)
	}

	private def createKeepAliveScheduler() : Cancellable ={
		context.system.scheduler.scheduleOnce(1 minutes, self, MyClosedByTimeout)(context.dispatcher)
	}

	private def processIncomingData( data: ByteString ): Unit ={

		keepAliveScheduler.cancel()
		keepAliveScheduler = createKeepAliveScheduler()

		currentData = currentData ++ data

		while (true) {
			val iter: ByteIterator = currentData.iterator
			if (messageLengthExpected > 0) {
				if (iter.len < messageLengthExpected)
					return
				val bytes : Array[Byte] = iter.getBytes(messageLengthExpected)

				val ois = new ObjectInputStream(new ByteArrayInputStream(bytes))
				try{
					val message = ois.readObject()
					log.info( s"message received> $message")

					message match {
						case MyKeepAliveMessage =>
							if (clientId == 0) // server mode
								self ! MySendKeepAlive
						case _ : MyMessage =>
							chatServer ! message
						case _ => log.info(s"unsupported message received")
					}

				}catch{
					case e : Exception => log.info(e.getMessage)
				}
				ois.close()

				currentData = currentData.slice(messageLengthExpected, currentData.length)
				messageLengthExpected = 0
			}else{
				if (iter.len < MyMessageConfig.protocolHeaderLength)
					return
				val protocolHeader = iter.getShort
				if (protocolHeader != MyMessageConfig.protocolHeader) {
					closeConnection()
					return
				}
				val protocolVersion = iter.getShort
				if (protocolVersion != MyMessageConfig.protocolVersion){
					closeConnection()
					return
				}

				messageLengthExpected = iter.getShort & 0xFFFF
				currentData = currentData.slice(6, currentData.length)
			}
		}
	}

	private def sendMessage(message : Serializable): Unit ={

		val stream = new ByteArrayOutputStream()
		val oos = new ObjectOutputStream(stream)
		oos.writeObject(message)
		oos.close()

		val bytes = stream.toByteArray
		if (bytes.length > 0xffff){
			log.info(s"To many bytes in message ${bytes.length}")
			return
		}

		val builder = new ByteStringBuilder
		builder.putShort(MyMessageConfig.protocolHeader)
		builder.putShort(MyMessageConfig.protocolVersion)
		builder.putShort(bytes.length)
		builder.putBytes(bytes)
		socketActor ! Write(builder.result())
	}

	private def closeConnection(): Unit ={
		log.info(s"Socket $clientId close connection")
		context.stop(socketActor)
	}

	private def socketClosed(): Unit ={
		log.info(s"Socket $clientId closed")
		chatServer ! MySessionClosed(clientId)
		context.stop(sender())
	}

	override def postStop(): Unit = {
		super.postStop()

		keepAliveScheduler.cancel()
		if (sendKeepAliveScheduler != null)
			sendKeepAliveScheduler.cancel()
	}


}
