package my

import java.io.{BufferedReader, InputStreamReader}

import akka.actor.{ActorRef, ActorSystem}

object MyServerApp extends App {
	private val actorSystem = ActorSystem("ServerSystem")

	private val myServer : ActorRef = actorSystem.actorOf(MyServer.props(), "MyServer")
	private val consoleListener : ActorRef = actorSystem.actorOf( MyConsoleListener.props(myServer, new BufferedReader( new InputStreamReader(System.in))), "ConsoleListener")
	myServer ! MyServerBind
	consoleListener ! MyStartServerConsole
}
