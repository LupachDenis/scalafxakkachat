package my

import java.io.{ByteArrayInputStream, ByteArrayOutputStream, ObjectInputStream, ObjectOutputStream}
import java.util.UUID

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.io.Tcp
import akka.testkit.{TestKit, TestProbe}
import akka.util.ByteString
import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, Matchers}


class MySessionSpec(_system : ActorSystem)
	extends TestKit(_system)
	with Matchers
	with FlatSpecLike
	with BeforeAndAfterAll
{
	def this() = this(ActorSystem("MySessionSpec"))

	override def afterAll: Unit ={
		shutdown(system)
	}

	class TestRepeater(  func : (TestRepeater, ByteString)=>Unit  ) extends Actor{

		var secondActor : ActorRef = _
		var dataBuffer : ByteString = _

		override def receive: Receive ={
			case c : ActorRef =>
				secondActor = c
			case Tcp.Write(data,_) =>
				func(this, data)
		}
	}

	case class TestProbeSample(testProbeSender : TestProbe, sessionSender : ActorRef,
							   testRepeater: ActorRef,
							   sessionReceiver : ActorRef, testProbeReceiver : TestProbe)

	private var currentProbeCount = 0

	def createTestProbe(func : (TestRepeater, ByteString)=>Unit) : TestProbeSample ={
		val testProbeSender = TestProbe()
		val testRepeater = system.actorOf(Props(new TestRepeater(func)), "TestRepeater" + currentProbeCount)
		val sessionSender = system.actorOf(Props(new MySession(testRepeater, testProbeSender.ref, 0)), "MySessionSender"+ currentProbeCount)

		val testProbeReceiver = TestProbe()
		val sessionReceiver = system.actorOf(Props(new MySession(testRepeater, testProbeReceiver.ref, 0)), "MySessionReceiver"+ currentProbeCount)

		currentProbeCount += 1

		testRepeater ! sessionReceiver

		TestProbeSample(testProbeSender, sessionSender, testRepeater, sessionReceiver, testProbeReceiver)
	}

	//val myTextMessage = MyTextMessage(1, MyClientIdName(2, "Name"), "Hello")
	val myTextMessage = MyHelloFromServerMessage(UUID.randomUUID(), List(MyClientIdName(1, "Name1"), MyClientIdName(2, "Name2")))

	"serialization" should "work" in{
		val stream = new ByteArrayOutputStream()
		val oos = new ObjectOutputStream(stream)
		oos.writeObject(myTextMessage)
		oos.close()

		val bytes = stream.toByteArray
		val ois = new ObjectInputStream(new ByteArrayInputStream(bytes))
		var message : AnyRef = null
		try{
			message = ois.readObject()
			println( message.toString )
		}catch{
			case e : Exception => println(e.getMessage)
		}
		val isEq1 = message==myTextMessage
		val isEq2 = message===myTextMessage
		(message==myTextMessage) should be (true)
	}

	"processIncomingData" should "receive 1 message" in{
		val testProbeSample = createTestProbe{
			( repeater : TestRepeater, byteString : ByteString ) =>
				repeater.secondActor ! Tcp.Received(byteString)
		}

		testProbeSample.testProbeSender.send(testProbeSample.sessionSender, myTextMessage)
		testProbeSample.testProbeReceiver.expectMsg(myTextMessage)
		testProbeSample.testProbeSender.send(testProbeSample.sessionSender, myTextMessage)
		testProbeSample.testProbeReceiver.expectMsg(myTextMessage)
	}

	"processIncomingData" should "receive 2 messages in one block" in{
		val testProbeSample = createTestProbe{
			( repeater : TestRepeater, byteString : ByteString ) =>
				if (repeater.dataBuffer == null) {
					repeater.dataBuffer = byteString
				}else {
					repeater.dataBuffer = repeater.dataBuffer ++ byteString
					repeater.secondActor ! Tcp.Received(repeater.dataBuffer)
					repeater.dataBuffer = null
				}
		}

		testProbeSample.testProbeSender.send(testProbeSample.sessionSender, myTextMessage)
		testProbeSample.testProbeSender.send(testProbeSample.sessionSender, myTextMessage)
		testProbeSample.testProbeReceiver.expectMsg(myTextMessage)
		testProbeSample.testProbeReceiver.expectMsg(myTextMessage)
	}

	"processIncomingData" should "receive less than 6 bytes and then others" in{
		val testProbeSample = createTestProbe{
			( repeater : TestRepeater, byteString : ByteString ) =>
				val sliceCount = 3
				repeater.secondActor ! Tcp.Received( byteString.slice(0,sliceCount) )
				repeater.secondActor ! Tcp.Received( byteString.slice(sliceCount,byteString.length) )
		}

		testProbeSample.testProbeSender.send(testProbeSample.sessionSender, myTextMessage)
		testProbeSample.testProbeReceiver.expectMsg(myTextMessage)
		testProbeSample.testProbeSender.send(testProbeSample.sessionSender, myTextMessage)
		testProbeSample.testProbeReceiver.expectMsg(myTextMessage)
	}

	"processIncomingData" should "receive more than 6 bytes and then others" in{
		val testProbeSample = createTestProbe{
			( repeater : TestRepeater, byteString : ByteString ) =>
				val sliceCount = 9
				repeater.secondActor ! Tcp.Received( byteString.slice(0,sliceCount) )
				repeater.secondActor ! Tcp.Received( byteString.slice(sliceCount,byteString.length) )
		}

		testProbeSample.testProbeSender.send(testProbeSample.sessionSender, myTextMessage)
		testProbeSample.testProbeReceiver.expectMsg(myTextMessage)
		testProbeSample.testProbeSender.send(testProbeSample.sessionSender, myTextMessage)
		testProbeSample.testProbeReceiver.expectMsg(myTextMessage)
	}

	// TODO : check wrong package

}
