name := "ScalaFxAkkaChat"

version := "1.2.1"

scalaVersion := "2.12.6"

// Scala FX
lazy val scalaFxVersion = "8.0.144-R12"
libraryDependencies += "org.scalafx" %% "scalafx" % scalaFxVersion

// Akka
lazy val akkaVersion = "2.5.14"

libraryDependencies += "com.typesafe.akka" %% "akka-actor" % akkaVersion
libraryDependencies += "com.typesafe.akka" %% "akka-testkit" % akkaVersion

// Test lib
lazy val scalaTestVersion = "3.0.5"
libraryDependencies += "org.scalactic" %% "scalactic" % scalaTestVersion
libraryDependencies += "org.scalatest" %% "scalatest" % scalaTestVersion % "test"

// Prevent startup bug in JavaFX
fork := true